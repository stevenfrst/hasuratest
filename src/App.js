import logo from './logo.svg';
import './App.css';
import {gql,useSubscription} from "@apollo/client";

const filterViaCategory = gql`
  subscription MySubscription($where: frontend_pos_menu_bool_exp = {}) {
    frontend_pos_menu(where: $where) {
      category
      harga
      nama
    }
  }
`



function App() {
  const {data,loading,error} = useSubscription(filterViaCategory,{})
  return (
    <>
      {
        data?.frontend_pos_menu.map((item)=>
          <ul>
            <li>{item.nama} {item.harga} {item.category}</li>
          </ul>
        )
      }
    </>
  );
}

export default App;
